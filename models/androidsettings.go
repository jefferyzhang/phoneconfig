package models

import (
	"errors"
	"github.com/jinzhu/gorm"
)

// AndroidSetting for main table
type AndroidSetting struct {
	gorm.Model
	Maker        string `gorm:"type:varchar(100);not null;" json:"maker" form:"maker"`
	MModel       string `gorm:"column:model;type:varchar(100)" json:"model" form:"model"`
	Version      string `gorm:"type:varchar(100);" json:"version" form:"version"`
	BuildVersion string `gorm:"column:buildversion;type:varchar(100);" json:"buildversion" form:"buildversion"`
	TypeData     int    `gorm:"not null;default:'0'" json:"type" form:"type"`
	Data         string `sql:"type:mediumtext;" json:"data"`
}

var (
	// ErrSettingsExist indicates setting is exist
	ErrSettingsExist = errors.New("AndroidSetting is exist")
	// ErrSettingsNotExist indicates setting is exist
	ErrSettingsNotExist = errors.New("AndroidSetting is not exist")
)

//AddNew add new record
func (as *AndroidSetting) AddNew() (err error) {
	if dbops.db.NewRecord(as) {
		err = dbops.db.Create(as).Error
	} else {
		err = ErrSettingsExist
	}
	return
}

//UpdateRecord one record
func (as *AndroidSetting) UpdateRecord(id uint) (err error) {
	var tmp AndroidSetting
	if !dbops.db.Where("id=?", id).First(&tmp).RecordNotFound() {
		as.ID = id
		err = dbops.db.Save(as).Error
	} else {
		err = ErrSettingsNotExist
	}
	return
}

//GetRecords many record
func (as *AndroidSetting) GetRecords() (ases []AndroidSetting, err error) {
	err = dbops.db.Debug().Where(as).Find(&ases).Error

	return
}

//DelRecord one record
func (as *AndroidSetting) DelRecord(id uint) (err error) {
	as.ID = id
	err = dbops.db.Unscoped().Delete(as).Error
	return
}

//DelMultiRecord one record
func (as *AndroidSetting) DelMultiRecord() (err error) {
	err = dbops.db.Unscoped().Where(as).Delete(&AndroidSetting{}).Error
	return
}