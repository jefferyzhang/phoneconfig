package models

import (
	"errors"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)
//User is user table
type User struct {
	gorm.Model         // fields `ID`, `CreatedAt`, `UpdatedAt`, `DeletedAt`will be added
	Name        string `gorm:"column:username" json:"username"`
	Email       string `gorm:"not null;unique" json:"email" valid:"Required;Email;MaxSize(100)"`
	Password    string `json:"-"`
	IsActivated bool   `gorm:"not null;default:0" json:"isactivated"`
	RawPSW      string `gorm:"-" json:"password" valid:"Required;MinSize(4)"`
}

var (
	// ErrUserExist indicates user is exist
	ErrUserExist = errors.New("User (email) is exist")
)
// HashPassword create password from RawPSW 
func (u *User) HashPassword() error {
	bytes, err := bcrypt.GenerateFromPassword([]byte(u.RawPSW), 14)
	if err == nil {
		u.Password = string(bytes)
	}
	return err
}
//CheckPasswordHash check password
func (u *User) CheckPasswordHash() error {
	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(u.RawPSW))
	return err
}
//Register create new user
func (u *User) Register() error {
	var err error
	if u.Password==""{
		err = u.HashPassword()
		if err!=nil{
			return err
		}
	}
	var temp User
	if dbops.db.Where("email=?",u.Email).First(&temp).RecordNotFound(){
		err = dbops.db.Create(&u).Error
		return err
	}
	return errors.New("access failed")
}

//Login login get user token(Cookie)
func (u *User) Login() error {
	var err error
	if u.Email == "" || u.RawPSW == "" {
		return errors.New("need mail and password param")
	}
	err = u.HashPassword()
	if err!=nil{
		return err
	}

	var temp User
	if err = dbops.db.Where("email=?",u.Email).First(&temp).Error;err==nil{
		temp.RawPSW = u.RawPSW
		if err=temp.CheckPasswordHash();err==nil{
			return nil
		}else{
			return errors.New("password failed")
		}
	}
	return err
}