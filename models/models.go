package models

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"log"
	"time"

	"PhoneConfig/config/setting"
)

//var db *gorm.DB

type dboperation struct {
	db *gorm.DB
}

var dbops = &dboperation{}

// Setup initializes the database instance
func Setup() {
	dbops.Setup(setting.Config.DB.Type,
		setting.Config.DB.Name,
		setting.Config.DB.User,
		setting.Config.DB.Password,
		setting.Config.DB.Host,
		setting.Config.DB.TablePrefix)
}
// Setup connect to MySql
func (dbs *dboperation) Setup(dbType, dbName, user, password, host, tablePrefix string) (err error) {

	//var err error
	cmdconnect := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8&parseTime=True&loc=Local",
		user,
		password,
		host,
		dbName)
	log.Println(cmdconnect)
	dbs.db, err = gorm.Open(dbType, cmdconnect)

	if err != nil {
		log.Println(err)
		return
	}

	gorm.DefaultTableNameHandler = func(db *gorm.DB, defaultTableName string) string {
		return tablePrefix + defaultTableName
	}

	dbs.db.SingularTable(true)
	//dbs.db.Callback().Create().Replace("gorm:update_time_stamp", updateTimeStampForCreateCallback)
	//dbs.db.Callback().Update().Replace("gorm:update_time_stamp", updateTimeStampForUpdateCallback)
	//dbs.db.Callback().Delete().Replace("gorm:delete", deleteCallback)
	dbs.db.DB().SetMaxIdleConns(10)
	dbs.db.DB().SetMaxOpenConns(100)
//User from 
	dbs.db.AutoMigrate(&User{}, &AndroidSetting{})
	
	return nil
}
//Close disconnect mysql
func (dbs *dboperation) Close() {
	dbs.db.Close()
}

// updateTimeStampForCreateCallback will set `CreatedOn`, `ModifiedOn` when creating
func updateTimeStampForCreateCallback(scope *gorm.Scope) {
	if !scope.HasError() {
		nowTime := time.Now().Unix()
		if createTimeField, ok := scope.FieldByName("CreatedAt"); ok {
			if createTimeField.IsBlank {
				createTimeField.Set(nowTime)
			}
		}

		if modifyTimeField, ok := scope.FieldByName("ModifiedAt"); ok {
			if modifyTimeField.IsBlank {
				modifyTimeField.Set(nowTime)
			}
		}
	}
}

// updateTimeStampForUpdateCallback will set `ModifiedOn` when updating
func updateTimeStampForUpdateCallback(scope *gorm.Scope) {
	if _, ok := scope.Get("gorm:update_column"); !ok {
		scope.SetColumn("ModifiedAt", time.Now().Unix())
	}
}

// deleteCallback will set `DeletedOn` where deleting
func deleteCallback(scope *gorm.Scope) {
	if !scope.HasError() {
		var extraOption string
		if str, ok := scope.Get("gorm:delete_option"); ok {
			extraOption = fmt.Sprint(str)
		}

		deletedOnField, hasDeletedOnField := scope.FieldByName("DeletedAt")

		if !scope.Search.Unscoped && hasDeletedOnField {
			scope.Raw(fmt.Sprintf(
				"UPDATE %v SET %v=%v%v%v",
				scope.QuotedTableName(),
				scope.Quote(deletedOnField.DBName),
				scope.AddToVars(time.Now().Unix()),
				addExtraSpaceIfExist(scope.CombinedConditionSql()),
				addExtraSpaceIfExist(extraOption),
			)).Exec()
		} else {
			scope.Raw(fmt.Sprintf(
				"DELETE FROM %v%v%v",
				scope.QuotedTableName(),
				addExtraSpaceIfExist(scope.CombinedConditionSql()),
				addExtraSpaceIfExist(extraOption),
			)).Exec()
		}
	}
}

// addExtraSpaceIfExist adds a separator
func addExtraSpaceIfExist(str string) string {
	if str != "" {
		return " " + str
	}
	return ""
}
