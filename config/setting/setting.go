package setting

import (
	"log"

	"github.com/pelletier/go-toml"
)

type APP struct {
	JwtSecret string `toml:"jwtsecret"`
	PageSize  int    `toml:"pagesize"`
	PrefixUrl string `toml:"prefixurl"`

	RuntimeRootPath string `toml:"runtimerootpath"`

	ImageSavePath  string   `toml:"imagesavepath"`
	ImageMaxSize   int      `toml:"imagemaxsize"`
	ImageAllowExts []string `toml:"imageallowexs"`

	ExportSavePath string `toml:"exportsavepath"`
	QrCodeSavePath string `toml:"qrcodesavepath"`
	FontSavePath   string `toml:"fontsavepath"`

	LogSavePath string `toml:"logsavepath"`
	LogSaveName string `toml:"logsavename"`
	LogFileExt  string `toml:"logfileext"`
	TimeFormat  string `toml:"timeformat"`
}

type ServerSetting struct {
	RunMode      string `toml:"runmode"`
	HttpPort     int    `toml:"httpport"`
	ReadTimeout  int    `toml:"readtimeout"`
	WriteTimeout int    `toml:"writetimeout"`
}

type Database struct {
	Type        string
	User        string
	Password    string
	Host        string
	Name        string
	TablePrefix string `toml:"tableprefix"`
}

type RedisSetting struct {
	Host        string
	Password    string
	MaxIdle     int `toml:"maxidle"`
	MaxActive   int `toml:"maxactive"`
	IdleTimeout int `toml:"idletimeout"`
}

type Configuration struct {
	App    APP           `toml:"app"`
	Server ServerSetting `toml:"server"`
	DB     Database      `toml:"database"`
	Redis  RedisSetting  `toml:"redis"`
}

var Config = Configuration{}

// Setup initialize the configuration instance
func Setup() {

	configs, err := toml.LoadFile("conf/app.toml")
	if err == nil {
		configs.Unmarshal(&Config)
		log.Println(Config.Server.RunMode)
	} else {
		log.Fatalf("setting.Setup, fail to parse 'conf/app.toml': %v", err)
	}

	Config.App.ImageMaxSize = Config.App.ImageMaxSize * 1024 * 1024
	// Config.server.ReadTimeout = Config.server.ReadTimeout * time.Second
	// Config.server.WriteTimeout = Config.server.WriteTimeout * time.Second
	// Config.redis.IdleTimeout = Config.redis.IdleTimeout * time.Second
}
