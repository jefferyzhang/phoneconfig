package main

import (
	"PhoneConfig/common/gredis"
	"PhoneConfig/config/logging"
	"PhoneConfig/config/setting"
	"PhoneConfig/models"
	"PhoneConfig/routers"
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/handlers"
	_ "github.com/mkevac/debugcharts"
	_ "net/http/pprof"

)

func init() {
	setting.Setup()
	logging.Setup()
	gredis.Setup()
	models.Setup()

	//util.Setup()/home/jeffery/Documents/goworks/RestApi/config/setting/setting.go
}

func main() {

	go func() {
		log.Println(http.ListenAndServe("localhost:6060", handlers.CompressHandler(http.DefaultServeMux)))
	}()

	router := routers.InitRouter()
	
	srv := &http.Server{
		Addr:    ":8080",
		Handler: router,
	}

	go func() {
		// service connections
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// 等待中断信号以优雅地关闭服务器（设置 5 秒的超时时间）
	signalChan := make(chan os.Signal)
	signal.Notify(signalChan, os.Interrupt)
	sig := <-signalChan
	log.Println("Get Signal:", sig)
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	log.Println("Server exiting")
}
