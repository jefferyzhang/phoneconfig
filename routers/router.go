package routers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"time"

	"PhoneConfig/config/setting"
	v1 "PhoneConfig/routers/api/v1"

	"PhoneConfig/middleware/myjwt"
)

// InitRouter @Summary InitRouter
// @Description InitRouter
// @Produce json
// @Param NULL
// @Success gin.Engine
func InitRouter() *gin.Engine {
	gin.ForceConsoleColor()
	r := gin.New()

	//r.Use(gin.Logger())
	r.Use(gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {
		// your custom format
		return fmt.Sprintf("%s - [%s] \"%s %s %s %d %s \"%s\" %s\"\n",
			param.ClientIP,
			param.TimeStamp.Format(time.RFC1123),
			param.Method,
			param.Path,
			param.Request.Proto,
			param.StatusCode,
			param.Latency,
			param.Request.UserAgent(),
			param.ErrorMessage,
		)
	}))

	r.Use(gin.Recovery())

	gin.SetMode(setting.Config.Server.RunMode)

	r.POST("/register", Register)
	r.POST("/login", Login)

	apiv1 := r.Group("/api/v1")
	apiv1.Use(myjwt.JWT())
	{
		//获取标签列表
		apiv1.GET("/settings", v1.GetSettings)
		//新建标签
		apiv1.POST("/setting", v1.AddSetting)
		//更新指定标签
		apiv1.PUT("/settings/:id", v1.UpdateSetting)
		//删除指定标签
		apiv1.DELETE("/setting/:regex", v1.DelSetting)
		apiv1.DELETE("/settings/delete", v1.DelSetting)
	}

	return r
}
