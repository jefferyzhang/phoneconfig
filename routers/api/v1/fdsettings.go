package v1

import (
	"net/http"
	"strconv"
	"regexp"

	"PhoneConfig/common/e"
	MDL "PhoneConfig/models"
	"PhoneConfig/routers/app"

	"github.com/gin-gonic/gin"
)

// GetSettings query all records with condition
// @Summary query  records
// @Description query records
// @Produce json
// @Param ?maker=&&model=&&version=...
// @Success 200 {string} string "ok" "return list datas"
// @Failure 400 {string} string "err_code：10002 参数错误； err_code：10003 校验错误"
// @Failure 401 {string} string "err_code：10001 登录失败"
// @Failure 500 {string} string "err_code：20001 服务错误；err_code：20002 接口错误；err_code：20003 无数据错误；err_code：20004 数据库异常；err_code：20005 缓存异常"
// @Router /api/v1/settings [get]
func GetSettings(c *gin.Context) {
	appG := app.Gin{C: c}
	var as MDL.AndroidSetting
	if c.ShouldBind(&as) == nil {
		ases, err := as.GetRecords()
		if err != nil {
			appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, err)
		} else {
			appG.Response(http.StatusOK, e.SUCCESS, ases)
		}
	} else {
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, "request error")
	}

}

// AddSetting add new setting
// @Summary add new setting
// @Description add new  setting to db
// @Produce json
// @Param json
// @Success 200 {string} string "ok" ""
// @Failure 400 {string} string "err_code：10002 参数错误； err_code：10003 校验错误"
// @Failure 401 {string} string "err_code：10001 登录失败"
// @Failure 500 {string} string "err_code：20001 服务错误；err_code：20002 接口错误；err_code：20003 无数据错误；err_code：20004 数据库异常；err_code：20005 缓存异常"
// @Router /api/v1/setting [post]
func AddSetting(c *gin.Context) {
	appG := app.Gin{C: c}
	var as MDL.AndroidSetting
	c.BindJSON(&as)
	if err := as.AddNew(); err != nil {
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, err)
	} else {
		appG.Response(http.StatusOK, e.SUCCESS, nil)
	}
}

// UpdateSetting Update setting 
// @Summary udpate setting
// @Description update setting need ID field
// @Produce json
// @Param json
// @Success 200 {string} string "ok" "返回用户信息"
// @Failure 400 {string} string "err_code：10002 参数错误； err_code：10003 校验错误"
// @Failure 401 {string} string "err_code：10001 登录失败"
// @Failure 500 {string} string "err_code：20001 服务错误；err_code：20002 接口错误；err_code：20003 无数据错误；err_code：20004 数据库异常；err_code：20005 缓存异常"
// @Router /v1/api/settings/{id} [PUT]
func UpdateSetting(c *gin.Context) {
}

// DelSetting delete records
// @Summary delete records
// @Description delete records
// @Produce json
// @Param body body controllers.LoginParams true "body参数"
// @Success 200 {string} string "ok" "返回用户信息"
// @Failure 400 {string} string "err_code：10002 参数错误； err_code：10003 校验错误"
// @Failure 401 {string} string "err_code：10001 登录失败"
// @Failure 500 {string} string "err_code：20001 服务错误；err_code：20002 接口错误；err_code：20003 无数据错误；err_code：20004 数据库异常；err_code：20005 缓存异常"
// @Router /api/v1/setting/{id} [DELETE]
// @Router /api/v1/setting/delete [DELETE]
func DelSetting(c *gin.Context) {
	appG := app.Gin{C: c}
	r, err := regexp.Compile(`\d+`)
    if err != nil {
       return
    }
	id := c.Param("regex")
	if r.MatchString(id) {
		nid, _ := strconv.ParseUint(id, 10, 64)
		var as MDL.AndroidSetting
		if err := as.DelRecord(uint(nid)); err != nil {
			appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, err)
		} else {
			appG.Response(http.StatusOK, e.SUCCESS, nil)
		}
	}else{
		var as MDL.AndroidSetting
		if c.ShouldBind(&as) == nil {
			err := as.DelMultiRecord()
			if err != nil {
				appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, err)
			} else {
				appG.Response(http.StatusOK, e.SUCCESS, nil)
			}
		} else {
			appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, "request error")
		}
	}
}
