package routers

import (
	"net/http"

	"PhoneConfig/common/e"
	MDL "PhoneConfig/models"
	"PhoneConfig/routers/app"

	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"

	"PhoneConfig/middleware/myjwt"
)

// Register @Summary Register
// @Description Register new User
// @Produce Post json
// @Param {username:"username", password:"password"}
// @Success 200 {string} string "ok" "返回用户信息"
// @Failure 400 {string} string "err_code：10002 参数错误； err_code：10003 校验错误"
// @Failure 401 {string} string "err_code：10001 登录失败"
// @Failure 500 {string} string "err_code：20001 服务错误；err_code：20002 接口错误；err_code：20003 无数据错误；err_code：20004 数据库异常；err_code：20005 缓存异常"
// @Router /register [post]
func Register(c *gin.Context) {
	appG := app.Gin{C: c}
	valid := validation.Validation{}
	var u MDL.User
	c.BindJSON(&u)

	ok, _ := valid.Valid(&u)

	if !ok {
		//app.MarkErrors(valid.Errors)
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, nil)
		return
	}

	if u.Register() != nil {
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, nil)
	} else {
		appG.Response(http.StatusOK, e.SUCCESS, nil)
	}
}

// Login @Summary Login
// @Description 登录
// @Produce json
// @Param body body controllers.LoginParams true "body参数"
// @Success 200 {string} string "ok" "返回用户信息"
// @Failure 400 {string} string "err_code：10002 参数错误； err_code：10003 校验错误"
// @Failure 401 {string} string "err_code：10001 登录失败"
// @Failure 500 {string} string "err_code：20001 服务错误；err_code：20002 接口错误；err_code：20003 无数据错误；err_code：20004 数据库异常；err_code：20005 缓存异常"
// @Router /login [post]
func Login(c *gin.Context) {
	appG := app.Gin{C: c}
	var u MDL.User
	valid := validation.Validation{}
	if c.ShouldBind(&u) == nil {
		ok, _ := valid.Valid(&u)
		if !ok {
			appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, nil)
			return
		}
		if err := u.Login(); err == nil {
			token, err := myjwt.GenerateToken(u.Email, u.Password)
			if (err!=nil){
				appG.Response(http.StatusUnauthorized, e.INVALID_PARAMS, nil)
				return
			}
			c.SetCookie("fd_jeffery", token, 3600, "/", "0.0.0.0", true, false)
			appG.Response(http.StatusOK, e.SUCCESS, nil)
		}else{
			appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, err)
		}
	} else {
		
		appG.Response(http.StatusBadRequest, e.INVALID_PARAMS, nil)
	}
	
}
