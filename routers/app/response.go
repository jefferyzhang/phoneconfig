package app

import (
	"PhoneConfig/common/e"
	"github.com/gin-gonic/gin"
)
//Gin from router to Response
type Gin struct {
	C *gin.Context
}
// Response return to client
type Response struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

// Response setting gin.JSON
func (g *Gin) Response(httpCode, errCode int, data interface{}) {
	g.C.JSON(httpCode, Response{
		Code: errCode,
		Msg:  e.GetMsg(errCode),
		Data: data,
	})
	return
}
